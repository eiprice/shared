from setuptools import setup
import os

list_dep = [
    'Unidecode',
    'nest_asyncio',
    'backoff',
    'requests-html',
    'user_agent'
]

setup(
    name='httpclient',
    version='1.0.0',
    packages=['eiprice'],
    install_requires=list_dep,
)
